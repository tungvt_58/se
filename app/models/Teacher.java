package models;

import com.avaje.ebean.Page;
import play.data.validation.Constraints;
import play.db.ebean.Model;
import play.mvc.PathBindable;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import java.util.List;

/**
 * Created by admin on 04/04/2015.
 */
@Entity
public class Teacher extends Model implements PathBindable<Teacher> {
    @Id
    public Long id;
    @Constraints.Required
    public String email;

    @Constraints.Required
    public String password;

    @Constraints.Required
    public String name;

    public String hocHam;
    public String hocVi;
    public String faculty;
    public String phoneNumber;
    public static String permission = "teacher";
    public Long idAccount;
    public String notes;


    public Teacher() {
    }

    public static Model.Finder<Long, Teacher> find = new Model.Finder<Long, Teacher>(Long.class, Teacher.class);

    public static Teacher findByEmail(String email) {
        return find.where().eq("email", email).findUnique();
    }

    public static Page<Teacher> find(int page) {  // trả về trang thay vì List
        return find.where()
                .orderBy("id asc")     // sắp xếp tăng dần theo id
                .findPagingList(8)    // quy định kích thước của trang
                .setFetchAhead(false)  // có cần lấy tất cả dữ liệu một thể?
                .getPage(page);    // lấy trang hiện tại, bắt đầu từ trang 0
    }

    public Teacher bind(String key, String value) {
        return findByEmail(value);
    }

    @Override
    public String unbind(String key) {
        return email;
    }

    @Override
    public String javascriptUnbind() {
        return email;
    }
}


