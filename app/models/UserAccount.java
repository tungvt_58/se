package models;

import play.data.validation.Constraints;
import play.db.ebean.Model;

import javax.persistence.Entity;
import javax.persistence.Id;

import javax.persistence.OneToOne;
import javax.validation.Constraint;

/**
 * Created by admin on 07/03/2015.
 */
@Entity
public class UserAccount extends Model {
    @Id
    public Long id;
    @Constraints.Required
    public String email;
    @Constraints.Required
    public String password;
    public String  permission;
    public Student student;

    public  UserAccount(){}

    public UserAccount(String email,String password,String permission){
        this.email=email;
        this.password= password;
        this.permission=permission;
    }
    public static Finder<Long, UserAccount> finder= new Finder(Long.class,UserAccount.class);

    public static UserAccount findByEmail(String email) {
        return finder.where().eq("email", email).findUnique();
    }

    public static UserAccount findById(Long id) {
        return finder.where().eq("id", id).findUnique();
    }
    public static UserAccount authenticate(String email, String password) {
        return finder.where().eq("email", email).eq("password", password).findUnique();
    }
}

