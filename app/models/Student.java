package models;

import com.avaje.ebean.Page;

import play.data.format.Formats;
import play.data.validation.Constraints;
import play.db.ebean.Model;
import play.mvc.PathBindable;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;

import java.util.Date;
import java.util.List;

/**
 * Created by admin on 07/03/2015.
 */
@Entity
public class Student extends Model implements PathBindable<Student> {
    @Id
    public Long id;

    @Constraints.Required
    public String name;
    @Constraints.Required
    public String email;
    @Constraints.Required
    public String password;
    @Constraints.Required
    public String stt;
    public String address;
    public String phoneNumber;
    public String faculty;
    public String course;
    public static String permission = "student";

    //@Formats.DateTime(pattern = "dd-MM-yyyy")
    public Date birthday;
    public String gender;
    public String notes;
    public Long idAccount;

    public Student() {
    }

    public static Model.Finder<Long, Student> find = new Model.Finder<Long, Student>(Long.class, Student.class);

    public static Student findByEmail(String email) {
        return find.where().eq("email", email).findUnique();
    }

    public static Student findByStt(String stt) {
        return find.where().eq("stt", stt).findUnique();
    }

    public static Page<Student> find(int page) {  // trả về trang thay vì List
        return find.where()
                .orderBy("id asc")     // sắp xếp tăng dần theo id
                .findPagingList(8)    // quy định kích thước của trang
                .setFetchAhead(false)  // có cần lấy tất cả dữ liệu một thể?
                .getPage(page);    // lấy trang hiện tại, bắt đầu từ trang 0
    }

    @Override
    public Student bind(String key, String value) {
        return findByEmail(value);
    }

    @Override
    public String unbind(String key) {
        return email;
    }

    @Override
    public String javascriptUnbind() {
        return email;
    }
}
