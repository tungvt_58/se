package controllers;

import com.avaje.ebean.Page;
import models.Student;
import models.UserAccount;
import play.data.Form;
import play.mvc.Controller;
import play.mvc.Result;
import play.mvc.Security;
import views.html.admin;
import views.html.creatForm;
import views.html.list;

import javax.servlet.annotation.ServletSecurity;
import java.util.List;

/**
 * Created by admin on 07/03/2015.
 */
@Security.Authenticated(Secured.class)
public class Admin extends Controller {

    public static Result home() {
        return ok(admin.render());
    }

    public static Result list(Integer page) {
        Page<Student> students = Student.find(page);
        return ok(list.render(students));
    }
    public static Result newStudent() {
        Form<Student> studentForm = Form.form(Student.class);
        return ok(creatForm.render(studentForm));
    }

    public static Result details(Student student) {
        Form<Student> studentForm = Form.form(Student.class);
        if (student == null) {
            return notFound(String.format("Student %s does not exist.", student.email));
        }
        Form<Student> filledForm = studentForm.fill(student);
        return ok(creatForm.render(filledForm));
    }

    public static Result save() {
        Form<Student> studentForm = Form.form(Student.class);
        Form<Student> boundForm = studentForm.bindFromRequest();
        if (boundForm.hasErrors()) {
            flash("error ", "Please correct the form below.");
            return badRequest(creatForm.render(boundForm));
        }
        Student student = boundForm.get();
        UserAccount account;
        if (student.id == null) {
            if (Student.findByStt(student.stt) != null) {
                flash("error", "Mã sinh viên  đã tồn tại! ");
                return badRequest(creatForm.render(boundForm));
            }
            if (Student.findByEmail(student.email) != null) {
                flash("error", "Email đã tồn tại nhập Email khác ! ");
                return badRequest(creatForm.render(boundForm));
            }

            account = new UserAccount(student.email, student.password, student.permission);
            account.save();
            student.idAccount = account.id;
            student.save();
            flash("success", String.format("Đã thêm Học viên %s", student.email));
        } else {
            Student stu = Student.find.byId(student.id);
            account = UserAccount.findById(stu.idAccount);
            account.email = student.email;
            account.password = student.password;
            account.update();
            student.update();
            flash("success", String.format("Học viên %s đã được cập nhật", account.email));
        }

        return redirect(routes.Admin.list(0));
    }

    public static Result delete(String email) {
        final Student student = Student.findByEmail(email);
        final UserAccount account = UserAccount.findByEmail(email);
        if (student == null) {
            return notFound(String.format("Học viên  %s không tồn tại.", email));
        }
        account.delete();
        student.delete();
        flash("success", "Tài khoản đã bị xóa ");
        return redirect(routes.Admin.list(0));
    }

}
