package controllers;

import models.Student;
import models.Teacher;
import models.UserAccount;
import play.mvc.Controller;
import play.mvc.Result;
import views.html.*;
import play.data.Form;
import views.html.CreateTeacher.teachers;
import views.html.students.formStudents;
import views.html.students.students;

public class Application extends Controller {

    public static Result index() {
        return ok(index.render("Your new application is ready."));
    }

    public static class Login {
        public String email;
        public String password;
    }

    public static Result login() {
        return ok(login.render(Form.form(Login.class)));
    }

    public static Result logout() {
        Form<Login> loginForm = Form.form(Login.class).bindFromRequest();
        session().remove("email");
        return ok(login.render(loginForm));
    }

    public static Result authenticate() {
        Form<Login> loginForm = Form.form(Login.class).bindFromRequest();
        String email = loginForm.get().email;
        String password = loginForm.get().password;

        if (UserAccount.authenticate(email, password) == null) {
            flash("error", "Invalid email and/or password");
            session().clear();
            return redirect(routes.Application.login());
        }
        if (UserAccount.findByEmail(email).permission.equals("student")) {
            session("email", email);
            session("name",Student.findByEmail(email).name);
            return ok(students.render());
        }
        if (UserAccount.findByEmail(email).permission.equals("teacher")) {
            session("email", email);
            session("name", Teacher.findByEmail(email).name);
            return ok(teachers.render());
        }

        session("email", email);
        return ok(admin.render());

    }
}
