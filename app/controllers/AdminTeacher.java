package controllers;

import com.avaje.ebean.Page;
import models.Teacher;
import models.UserAccount;
import play.data.Form;
import play.mvc.Controller;
import play.mvc.Result;
import play.mvc.Security;
import views.html.CreateTeacher.teachersForm;
import views.html.CreateTeacher.teachersList;

/**
 * Created by admin on 04/04/2015.
 */
@Security.Authenticated(Secured.class)
public class AdminTeacher extends Controller {

    public static Result listTeacher(Integer page) {
        Page<Teacher> teachers = Teacher.find(page);
        return ok(teachersList.render(teachers));
    }

    public static Result newTeacher() {
        Form<Teacher> teacherForm = Form.form(Teacher.class);
        return ok(teachersForm.render(teacherForm));
    }

    public static Result details(Teacher teacher) {
        Form<Teacher> teachersForm = Form.form(Teacher.class);

        if (teacher == null) {
            return notFound(String.format("Giáo viên %s không tồn tại.", teacher.email));
        }
        Form<Teacher> filledForm = teachersForm.fill(teacher);
        return ok(views.html.CreateTeacher.teachersForm.render(filledForm));
    }

    public static Result save() {
        Form<Teacher> teachersForm = Form.form(Teacher.class);
        Form<Teacher> bound = teachersForm.bindFromRequest();
        if (bound.hasErrors()) {
            flash("error", "Please crrect the form below.");
            return badRequest(views.html.CreateTeacher.teachersForm.render(bound));
        }
        Teacher teacher = bound.get();
        UserAccount account;
        if (teacher.id == null) {
            if (Teacher.findByEmail(teacher.email) != null) {
                flash("error", "Email đã tồn tại nhập Email khác ! ");
                return badRequest(views.html.CreateTeacher.teachersForm.render(bound));
            }

            account = new UserAccount(teacher.email, teacher.password, teacher.permission);
            account.save();
            teacher.idAccount = account.id;
            teacher.save();
            flash("success", String.format("Thêm giáo viên %s ", teacher.email));
        } else {
            Teacher te = Teacher.find.byId(teacher.id);
            account = UserAccount.findById(te.idAccount);
            account.email = teacher.email;
            account.password = teacher.password;
            account.update();
            teacher.update();
            flash("success", String.format("Cập nhật giáo viên %s", teacher.email));
        }
        return redirect(routes.AdminTeacher.listTeacher(0));
    }

    public static Result delete(String email) {
        final Teacher teacher = Teacher.findByEmail(email);
        final UserAccount account = UserAccount.findByEmail(email);
        if (teacher == null) {
            return notFound(String.format("Giáo viên %s không tồn tại.", email));
        }
        account.delete();
        teacher.delete();
        flash("success", "Đã xóa giáo viên ");
        return redirect(routes.AdminTeacher.listTeacher(0));
    }
}
