package controllers;

import models.Teacher;
import models.UserAccount;
import play.mvc.Result;
import play.data.Form;
import play.mvc.Controller;
import play.mvc.Security;
import views.html.CreateTeacher.teachers;
import views.html.CreateTeacher.teachersEdit;
import views.html.CreateTeacher.teachersForm;

/**
 * Created by admin on 03/05/2015.
 */
@Security.Authenticated(Secured.class)
public class Teachers extends Controller{
    public static Result index() {
        return ok(teachers.render());
    }
    public static Result details() {

        Form<Teacher> teacherForm = Form.form(Teacher.class);
        String email = session().get("email");

        Teacher teacher = Teacher.findByEmail(email);
        Form<Teacher> filledForm = teacherForm.fill(teacher);

        return ok(teachersEdit.render(filledForm));
    }
    public static Result save() {
        Form<Teacher> teachersForm = Form.form(Teacher.class);
        Form<Teacher> bound = teachersForm.bindFromRequest();
        if (bound.hasErrors()) {
            flash("error", "Please crrect the form below.");
            return badRequest(views.html.CreateTeacher.teachersForm.render(bound));
        }
        Teacher teacher = bound.get();
        UserAccount account;
        if (teacher.id == null) {
            if (Teacher.findByEmail(teacher.email) != null) {
                flash("error", "Email đã tồn tại nhập Email khác ! ");
                return badRequest(views.html.CreateTeacher.teachersForm.render(bound));
            }

            account = new UserAccount(teacher.email, teacher.password, teacher.permission);
            account.save();
            teacher.idAccount = account.id;
            teacher.save();
            flash("success", String.format("Thêm giáo viên %s ", teacher.email));
        } else {
            Teacher te = Teacher.find.byId(teacher.id);
            account = UserAccount.findById(te.idAccount);
            account.email = teacher.email;
            account.password = teacher.password;
            account.update();
            teacher.update();
            flash("success", String.format("Thông tin được cập nhật", teacher.email));
        }
        return redirect(routes.Students.index());
    }
}
