package controllers;

import models.Student;
import models.UserAccount;
import play.mvc.Result;
import play.data.Form;
import play.mvc.Controller;
import play.mvc.Security;
import views.html.students.formStudents;
import views.html.students.students;
import views.html.students.view;

/**
 * Created by admin on 15/03/2015.
 */
@Security.Authenticated(Secured.class)

public class Students extends Controller {
    public static Result index() {
        return ok(students.render());
    }
    public static Result information(){
        Form<Student> studentForm = Form.form(Student.class);
        String email = session().get("email");

        Student student = Student.findByEmail(email);
        Form<Student> filledForm = studentForm.fill(student);

        return ok(view.render(filledForm));
    }

    public static Result details() {

        Form<Student> studentForm = Form.form(Student.class);
        String email = session().get("email");

        Student student = Student.findByEmail(email);
        Form<Student> filledForm = studentForm.fill(student);

        return ok(formStudents.render(filledForm));
    }
    public static Result save() {
        Form<Student> studentForm = Form.form(Student.class);
        Form<Student> boundForm = studentForm.bindFromRequest();
        if (boundForm.hasErrors()) {
            flash("error ", "Please correct the form below.");
            return badRequest(formStudents.render(boundForm));
        }
        Student student = boundForm.get();
        UserAccount account;
        if (student.id == null) {
            if (Student.findByStt(student.stt) != null) {
                flash("error", "Mã sinh viên  đã tồn tại! ");
                return badRequest(formStudents.render(boundForm));
            }
            if (Student.findByEmail(student.email) != null) {
                flash("error", "Email đã tồn tại nhập Email khác ! ");
                return badRequest(formStudents.render(boundForm));
            }

            account = new UserAccount(student.email, student.password, student.permission);
            account.save();
            student.idAccount = account.id;
            student.save();
            flash("success", String.format("Successfully added student %s", student.email));
        } else {
            Student stu = Student.find.byId(student.id);
            account = UserAccount.findById(stu.idAccount);
            account.email = student.email;
            account.password = student.password;
            account.update();
            student.update();
            flash("success", String.format("Successfully updated student %s", account.email));
        }

        return redirect(routes.Students.index());
    }
}
